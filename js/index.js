$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2500
    })
  });

function agregarClase(id){
    $('#reservaModal').on('show.bs.modal', function(e){
        console.log("el modal contacto se esta mostrando");
        $("#"+id).removeClass("btn-success");
        $("#"+id).addClass("btn-secondary");
        $("#"+id).prop("disabled",true);
    });
    $('#reservaModal').on("shown.bs.modal", function(e){
        console.log("el modal contacto se mostró");
    });
    $('#reservaModal').on("hide.bs.modal", function(e){
        console.log("el modal contacto se esta ocultando");
        $("#"+id).removeClass("btn-secondary");
        $("#"+id).addClass("btn-success");
        $("#"+id).prop("disabled",false);
    });
    $('#reservaModal').on("hidden.bs.modal", function(e){
        console.log("el modal contacto se ocultó");
    });



};