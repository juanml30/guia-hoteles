'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var del = require('del');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var usemin = require('gulp-usemin');
var rev = require('gulp-rev');
var cleanCss = require('gulp-clean-css');
var flatmap = require('gulp-flatmap');
var htmlmin = require('gulp-htmlmin');
    
sass.compiler = require('node-sass');
    
gulp.task('sass', function () {
    return gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream());
});
    
// Static Server and watching scss/html files
gulp.task('browser-sync', function (done) {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("css/*.scss", gulp.series('sass'));
    gulp.watch("*.html", browserSync.reload);
    gulp.watch("*.{png, jpg, gif}", browserSync.reload); // Me falta saber si esto asi funciona, pero hasta aca todo bien
    done();
});

gulp.task('default', gulp.series('sass', 'browser-sync'));

gulp.task('clean', function(done){
    del(['dist'])
    done();
});

gulp.task('copyfonts', function(done){
    gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
    .pipe(gulp.dest('./dist/fonts'))
    done();
})

gulp.task('imagemin',function(done){
        gulp.src('images/*')
            .pipe(imagemin())
            .pipe(gulp.dest('dist/images'))
            done();
});

gulp.task('usemin',function(){
    return gulp.src('./*.html')
    .pipe(flatmap(function(stream,file){
        return stream
        .pipe(usemin({
            css: [rev()],
            html: [ htmlmin({ collapseWhitespace: true }) ],
            js: [ uglify(), rev() ],
            inlinejs: [ uglify() ],
            inlinecss: [ cleanCss(), 'concat' ]
        }));
    }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('build', gulp.series('clean','copyfonts','imagemin','usemin') )


